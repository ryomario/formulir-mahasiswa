const form = require('./formulir-mahasiswa')
const readline = require('node:readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
const util = require('node:util');
const question = util.promisify(rl.question).bind(rl);

const input = async (label,options = {validation : (text)=>text.length>0,errMessage:"Field tidak boleh kosong!"}) => {
    let inputText = '';
    // First value true so the message not show on first access
    let isValid = true;
    do {
        inputText = await question(label)

        // In this state 
        isValid = options.validation(inputText)
        if (!isValid) {
            if (options.errMessage == undefined) options.errMessage = "Unknown!"
            // console.error(options.errMessage)
            throw new Error(options.errMessage)
        }
    } while(!isValid) 

    return inputText
}


// mahasiswa {
//     nama,
//     nim,
//     prodi,
//     jenjang,
//     sex
// }

const main = async () => {
    try {
        const mahasiswa = {}

        console.log("======= Formulir Mahasiswa =======")
        mahasiswa.nama = await input('Nama\t\t: ',{
            validation: (text) => text.trim().length > 2,
            errMessage: "Nama harus lebih dari 2 karakter!!!"
        });
        mahasiswa.nim = await input('NIM\t\t: ',{
            validation: (text) => text.trim().length > 8,
            errMessage: "NIM harus lebih dari 8 karakter!!!"
        });
        mahasiswa.prodi = await input('Program Studi\t: ',{
            validation: (text) => text.trim().length > 0,
            errMessage: "Program Studi tidak boleh kosong!!!"
        });
        mahasiswa.jenjang = await input('Jenjang\t\t: ',{
            validation: (text) => text.trim().length > 0,
            errMessage: "Jenjang tidak boleh kosong!!!"
        });
        mahasiswa.sex = await input('Jenis Kelamin\t: ',{
            // No validation for sex
            validation: (text) => true,
            // errMessage: ""
        });

        form.save(mahasiswa)
    } catch (err) {
        console.error(err)
    } finally {
        rl.close()
    }
}

main()

const fs = require('fs')

const directPath = './data/'
const pathJson = directPath+'/data-mahasiswa.json';

// cek folder jika ada dibuat jika tidak lanjut
if(!fs.existsSync(directPath)){
    fs.mkdirSync(directPath);
} 

if(!fs.existsSync(pathJson)) {
    fs.writeFileSync(pathJson, '[]', 'utf-8');
}

// load data.json
const loadAll = () => {
    // membaca file json (./data/data-mahasiswa.json)
    const fileBuffer = fs.readFileSync(pathJson, 'utf-8');
    return list_mahasiswa = JSON.parse(fileBuffer);
};

// Menyimpan Data

// mahasiswa {
//     nama,
//     nim,
//     prodi,
//     jenjang,
//     sex
// }
exports.save = (mahasiswa) => {
    loadAll();
    // cek duplikat nim mahasiswa
    const duplicates = list_mahasiswa.find(item => item.nim === mahasiswa.nim);
    if(duplicates){
        console.log(`Mahasiswa dengan NIM ${mahasiswa.nim} sudah terdaftar silahkan masukkan mahasiswa lain!!`);
        return false;
    };
    
    // menambah data yang pada variable json
    list_mahasiswa.push(mahasiswa);
  
    // menuliskan ke data-mahasiswa.json
    fs.writeFile(pathJson, JSON.stringify(list_mahasiswa, null, 2), (err) => {
        if(err) throw err;
        console.log('\n===> data telah tersipman <===');
        console.table(list_mahasiswa);
    });
    
    // readline ditutup / berakhir
  }
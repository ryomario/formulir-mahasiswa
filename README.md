# Formulir Mahasiswa

## Installation

Formulir untuk mengisi data mahasiswa, data akan disimpan di komputer lokal tepatnya di workdir aplikasi.

### Prepare

```bash
# clone repo
git clone https://gitlab.com/ryomario/formulir-mahasiswa.git

# masuk ke folder 'formulir-mahasiswa'
cd formulir-mahasiswa

# install package NodeJS yang diperlukan
npm i
```

### Running
```bash
# run application
npm start
```

## Contributors

| Employee ID | Name |
| --:|:--- |
| 2729125 | Mario |
